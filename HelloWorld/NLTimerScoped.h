#pragma once

#include <chrono>
#include <string>
#include <iostream>


class NLTimerScoped {
private:
    const std::chrono::steady_clock::time_point start;
    const std::string name;
    
public:
    NLTimerScoped( const std::string & name ) : name( name ), start( std::chrono::steady_clock::now() ) {
    }

    
    ~NLTimerScoped() {
        const auto end(std::chrono::steady_clock::now());
        const auto duration_us = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        
        std::cout << name << " duration: " << (double)duration_us / 1000.0 << "ms" << std::endl;
    }

};
