//
// Created by Damian Stewart on 08/07/16.
//

#include <iostream>
#include "OpenCLProgram.h"

using std::cerr;
using std::cout;
using std::endl;

static cl_context CreateOpenCLContext() {
    cl_int err;

    cl_device_id deviceId;
    cl_platform_id  platformId;
    cl_uint numPlatforms;
    err = clGetPlatformIDs(1, &platformId, &numPlatforms);
    if (err != CL_SUCCESS) {
        cerr << "clPlatformGetIDs fail: " << err << endl;
        return NULL;
    }

    cl_uint numDevices;
    err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_GPU, 1, &deviceId, &numDevices);
    if (err != CL_SUCCESS) {
        if (err == CL_DEVICE_NOT_FOUND) {
            cout << "OpenCL failed to get GPU device, trying DEFAULT" << endl;
            err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_DEFAULT, 1, &deviceId, &numDevices);
            if (err != CL_SUCCESS) {
                cerr << "clGetDeviceIDs fail: " << err << endl;
                return NULL;
            }
        }
    }

    char name[128];
    clGetDeviceInfo(deviceId, CL_DEVICE_NAME, 128, name, NULL);
    cout << "OpenCL device name: " << name << endl;

    auto context = clCreateContext(NULL, 1, &deviceId, NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        cerr << "clCreateContext fail: " << err << endl;
        return NULL;
    }

    return context;


}
bool OpenCLProgram::SetupContext() {
    auto context = CreateOpenCLContext();
    if (context == nullptr) {
        return false;
    }
    mContext = context;
    return true;
}

static cl_program LoadOpenCLProgram(cl_context context, cl_device_id deviceId, const GLchar* programCode) {

    cl_int err;
    cl_program program = clCreateProgramWithSource(context, 1, &programCode, NULL, &err);

    if (err != CL_SUCCESS) {
        cerr << "clCreateProgramWithSource fail: " << err << endl;
        return NULL;
    }

    const char *options = "";
    err = clBuildProgram(program, 1, &deviceId, options, NULL, NULL);
    if (err != CL_SUCCESS) {
        cl_build_status status;
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL);

        size_t logSize;
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
        auto log = (char *) calloc(logSize + 1, sizeof(char));
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, logSize + 1, log, NULL);
        cout << "OpenCL build failed: error " << err << ", status " << status << ", log:" << endl;
        cout << log << endl;
        free(log);
    }

    return program;
}

cl_device_id OpenCLProgram::GetDeviceID() {
    cl_device_id id;
    auto err = clGetContextInfo(mContext, CL_CONTEXT_DEVICES, 1, &id, NULL);
    if (err != CL_SUCCESS) {
        cerr << "unable to get device id: err " << err << endl;
    }
    return id;
}

bool OpenCLProgram::BuildProgram(const char *source) {

    auto program = LoadOpenCLProgram(mContext, GetDeviceID(), source);
    if (program == nullptr) {
        cerr << "error building program" << endl;
        return false;
    }

    cl_int err;
    auto kernel = clCreateKernel(program, "square", &err);
    if (err != CL_SUCCESS) {
        cerr << "Error creating kernel: " << err << endl;
        return false;
    }
    mKernel = kernel;

    auto queue = clCreateCommandQueue(mContext, GetDeviceID(), 0, &err);
    if (err != CL_SUCCESS) {
        cerr << "Error creating command queue: " << err << endl;
        return false;
    }
    mQueue = queue;

}

void OpenCLProgram::AddBufferKernelArgument(int argumentIndex, shared_ptr<OpenCLBuffer> buffer) {
    mBufferKernelArguments.push_back(buffer);
    auto err = clSetKernelArg(mKernel, argumentIndex, sizeof(cl_mem), buffer->GetBuffer());
    if (err != CL_SUCCESS) {
        cerr << "error adding buffer argument: " << err << endl;
    }
}

shared_ptr<OpenCLProgram> OpenCLProgram::CreateProgram(const char *source) {
    auto program = std::make_shared<OpenCLProgram>();
    bool success = program->SetupContext();
    if (!success) {
        return nullptr;
    }
    success = program->BuildProgram(source);
    if (!success) {
        return nullptr;
    }

    return program;
}

OpenCLProgram::~OpenCLProgram() {
    mBufferKernelArguments.clear();

    clReleaseCommandQueue(mQueue);
    clReleaseKernel(mKernel);
    clReleaseProgram(mProgram);
    clReleaseContext(mContext);
}

void OpenCLProgram::ExecuteKernel() {

}


OpenCLBuffer::OpenCLBuffer(cl_context context, cl_mem_flags flags, size_t dataSize)
: mDataSize(dataSize)
{
    mBuffer = clCreateBuffer(context, flags, dataSize, NULL, NULL);
}

OpenCLBuffer::~OpenCLBuffer() {
    clReleaseMemObject(mBuffer);
}

void OpenCLBuffer::EnqueueWrite(cl_command_queue queue, void *sourceData) {
    auto blocking = CL_TRUE;
    clEnqueueWriteBuffer(queue, mBuffer, blocking, 0, mDataSize, sourceData, 0, NULL, NULL);
}

void OpenCLBuffer::EnqueueRead(cl_command_queue queue, void *destination) {
    auto blocking = CL_TRUE;
    clEnqueueReadBuffer(queue, mBuffer, blocking, 0, mDataSize, destination, 0, NULL, NULL);
}







