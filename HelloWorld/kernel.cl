
const GLchar* SQUARE_KERNEL = R"(

__kernel void square(global float* input, global float* output) {
    size_t i = get_global_id(0);
    output[i] = input[i] * input[i];
//output[i] = sqrt(input[i]);
}
)";

const GLchar* HELLOWORLD_KERNEL = R"(

__kernel void hello(__global char* message){
    message[0] = 'H';
    message[1] = 'e';
    message[2] = 'l';
    message[3] = 'l';
    message[4] = 'o';
    message[5] = ',';
    message[6] = ' ';
    message[7] = 'W';
    message[8] = 'o';
    message[9] = 'r';
    message[10] = 'l';
    message[11] = 'd';
    message[12] = '!';
    message[13] = 0;
}
)";