#include <iostream>
#include <OpenCL/opencl.h>
#include <cmath>

#include "kernel.cl"
#include "NLTimerScoped.h"

using namespace std;

static const int NUM_VALUES = 1024*1024*128;

static cl_context CreateOpenCLContext() {
    cl_int err;

    cl_device_id deviceId;
    cl_platform_id  platformId;
    cl_uint numPlatforms;
    err = clGetPlatformIDs(1, &platformId, &numPlatforms);
    if (err != CL_SUCCESS) {
        cerr << "clPlatformGetIDs fail: " << err << endl;
        return NULL;
    }

    cl_uint numDevices;
    err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_GPU, 1, &deviceId, &numDevices);
    if (err != CL_SUCCESS) {
        if (err == CL_DEVICE_NOT_FOUND) {
            cout << "OpenCL failed to get GPU device, trying DEFAULT" << endl;
            err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_DEFAULT, 1, &deviceId, &numDevices);
            if (err != CL_SUCCESS) {
                cerr << "clGetDeviceIDs fail: " << err << endl;
                return NULL;
            }
        }
    }

    char name[128];
    clGetDeviceInfo(deviceId, CL_DEVICE_NAME, 128, name, NULL);
    cout << "OpenCL device name: " << name << endl;

    auto context = clCreateContext(NULL, 1, &deviceId, NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        cerr << "clCreateContext fail: " << err << endl;
        return NULL;
    }

    return context;
}


static cl_program LoadOpenCLProgram(cl_context context, cl_device_id deviceId, const GLchar* programCode) {

    cl_int err;
    cl_program program = clCreateProgramWithSource(context, 1, &programCode, NULL, &err);

    if (err != CL_SUCCESS) {
        cerr << "clCreateProgramWithSource fail: " << err << endl;
        return NULL;
    }

    const char* options = "";
    err = clBuildProgram(program, 1, &deviceId, options, NULL, NULL);
    if (err != CL_SUCCESS) {
        cl_build_status status;
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL);

        size_t logSize;
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
        auto log = (char*)calloc(logSize + 1, sizeof(char));
        clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, logSize+1, log, NULL);
        cout << "OpenCL build failed: error " << err << ", status " << status << ", log:" << endl;
        cout << log << endl;
        free(log);
    }

    return program;
}

static bool validate(cl_float* input, cl_float* output) {
    bool success = true;

    for (int i=0; i<NUM_VALUES; i++) {
        if (output[i] != input[i] * input[i]) {
            cerr<<"Error: element " << i << " mismatch (expected " << input[i] * input[i] << ", got " <<output[i] << endl;
            success = false;
        }
    }

    return success;
}


int main() {

    auto context = CreateOpenCLContext();
    cl_device_id deviceId;
    clGetContextInfo(context, CL_CONTEXT_DEVICES, sizeof(cl_device_id), &deviceId, NULL);
    auto program = LoadOpenCLProgram(context, deviceId, SQUARE_KERNEL);

    cl_build_status buildStatus;
    clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_STATUS, sizeof(CL_PROGRAM_BUILD_STATUS), &buildStatus, NULL);
    if (buildStatus != CL_BUILD_SUCCESS) {
        cerr << "Build status is " << buildStatus << " - exiting" << endl;
        return 1;
    }



    cl_int err;
    auto kernel = clCreateKernel(program, "square", &err);
    if (err != CL_SUCCESS) {
        cerr << "Error creating kernel: " << err << endl;
        return 1;
    }
    auto queue = clCreateCommandQueue(context, deviceId, 0, &err);
    if (err != CL_SUCCESS) {
        cerr << "Error creating command queue: " << err << endl;
        return 1;
    }


    size_t dataSize = NUM_VALUES * sizeof(cl_float);
    auto input = clCreateBuffer(context, CL_MEM_READ_ONLY, dataSize, NULL, NULL);
    auto output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, dataSize, NULL, NULL);
    if (input == nullptr || output == nullptr) {
        cerr << "unable to create buffers" << endl;
        if (input != nullptr) {
            clReleaseMemObject(input);
        }
        if (output != nullptr) {
            clReleaseMemObject(output);
        }
        return 1;
    }

    cl_float* values = (cl_float*)malloc(dataSize);
    for (int i=0; i<NUM_VALUES; i++) {
        values[i] = cl_float(i);
    }
    cl_float* squares = (cl_float*)malloc(dataSize);

    err = clEnqueueWriteBuffer(queue, input, CL_TRUE, 0, dataSize, values, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        cerr << "error enqueueing input: " << err << endl;
        return 1;
    }

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
    if (err != CL_SUCCESS) {
        cerr << "error setting kernel arg 0: " << err << endl;
        return 1;
    }
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
    if (err != CL_SUCCESS) {
        cerr << "error setting kernel arg 1: " << err << endl;
        return 1;
    }

    size_t localDomainSize;
    err = clGetKernelWorkGroupInfo(kernel, deviceId, CL_KERNEL_WORK_GROUP_SIZE, sizeof(localDomainSize), &localDomainSize, NULL);
    if (err != CL_SUCCESS) {
        cerr << "error getting kernel work group info: " << err << endl;
        return 1;
    }

    size_t globalDomainSize = NUM_VALUES;
    {
        NLTimerScoped timer("cl");
        err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalDomainSize, &localDomainSize, 0, NULL, NULL);
        if (err != CL_SUCCESS) {
            cerr << "error enqueueing kernel: " << err << endl;
            return 1;
        }
        cout << "kernel enqueued; domain sizes: local: " << localDomainSize << " global: " << globalDomainSize << endl;

        clFinish(queue);

        err = clEnqueueReadBuffer(queue, output, CL_TRUE, 0, dataSize, squares, 0, NULL, NULL);
        if (err != CL_SUCCESS) {
            cerr << "error reading results: " << err << endl;
            return 1;
        }
    }

    /*if (!validate(values, squares)) {
        cerr << "failed" << endl;
    } else {
        cout << "succeeded" << endl;
    }*/
    cout << "done" << endl;


    {
        NLTimerScoped timer("normal");
        for (int i = 0; i < NUM_VALUES; i++) {
            squares[i] = values[i]*values[i];
        }
    }


    clReleaseMemObject(input);
    clReleaseMemObject(output);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    free(values);
    free(squares);

    return 0;
}