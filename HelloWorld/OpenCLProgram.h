//
// Created by Damian Stewart on 08/07/16.
//

#ifndef HELLOWORLD_OPENCLPROGRAM_H
#define HELLOWORLD_OPENCLPROGRAM_H


#include <OpenCL/OpenCL.h>

#include <vector>

using std::shared_ptr;
using std::vector;

class OpenCLBuffer {
public:
    OpenCLBuffer(cl_context context, cl_mem_flags flags, size_t dataSize);
    ~OpenCLBuffer();

    void EnqueueWrite(cl_command_queue queue, void *sourceData);
    void EnqueueRead(cl_command_queue queue, void* destination);

    cl_mem GetBuffer() { return mBuffer; }

private:

    size_t mDataSize;
    cl_mem mBuffer;

};

class OpenCLProgram {

public:
    static shared_ptr<OpenCLProgram> CreateProgram(const char* source);


    void AddBufferKernelArgument(int argumentIndex, shared_ptr<OpenCLBuffer> buffer);

    void ExecuteKernel();

private:
    ~OpenCLProgram();

    bool SetupContext();
    bool BuildProgram(const char* source);

    cl_device_id GetDeviceID();

    cl_context mContext;
    cl_program mProgram;
    cl_kernel mKernel;
    cl_command_queue mQueue;

    vector<shared_ptr<OpenCLBuffer>> mBufferKernelArguments;

};


#endif //HELLOWORLD_OPENCLPROGRAM_H
